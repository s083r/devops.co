import argparse


def serve():
    print('serve')
    import service
    service.main()


def init():
    print('init')
    import main


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--serve', action="store_true")
    args = parser.parse_args()
    if args.serve:
        serve()
    else:
        init() 
