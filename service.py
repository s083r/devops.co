import http.server
import base64
import json
from urllib.parse import urlparse, parse_qs
import os
import psutil
import git
import sys

def get_sha():
    local_repo = git.Repo(search_parent_directories=True)
    sha = local_repo.head.object.hexsha
    return sha

def get_stats():
    process = psutil.Process(os.getpid())
    return process.pid, process.cpu_percent(), process.memory_percent()

class CustomServerHandler(http.server.BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header(
            'WWW-Authenticate', 'Basic realm="Demo Realm"')
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        global current_sha
        key = self.server.get_auth_key()
        if self.headers.get('Authorization') == None:
            self.do_AUTHHEAD()
            response = {
                'success': False,
                'error': 'No auth header received'
            }
            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
        elif self.headers.get('Authorization') == 'Basic ' + str(key):
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()

            if current_sha == get_sha():
                sha_state = 'equal'
            else:
                sha_state = 'differ'
                os.execv(sys.executable, ['python3'] + sys.argv)

            response = {
                'pid': str(get_stats()[0]),
                'cpu': str(round(get_stats()[1],2)) + '%',
                'ram': str(round(get_stats()[2],2)) + '%',
                'current sha': str(get_sha()),
                'on-run sha': str(current_sha),
                'state': sha_state
            }
            self.wfile.write(bytes(json.dumps(response), 'utf-8'))
        else:
            self.do_AUTHHEAD()
            response = {
                'success': False,
                'error': 'Invalid credentials'
            }
            self.wfile.write(bytes(json.dumps(response), 'utf-8'))

    def _parse_GET(self):
        getvars = parse_qs(urlparse(self.path).query)
        return getvars

class CustomHTTPServer(http.server.HTTPServer):
    key = ''

    def __init__(self, address, handlerClass=CustomServerHandler):
        super().__init__(address, handlerClass)
    def set_auth(self, username, password):
        self.key = base64.b64encode(
            bytes('%s:%s' % (username, password), 'utf-8')).decode('ascii')
    def get_auth_key(self):
        return self.key

def main():
    global current_sha
    current_sha = get_sha()
    server = CustomHTTPServer(('', 80))
    server.set_auth('demo1', 'demo1')
    server.serve_forever()

if __name__ == '__main__':
    main()
