"""
Test task for DevOps position

"""

import paramiko
import os
from pathlib import Path
import configparser
import boto3
from botocore.exceptions import ClientError

region = 'us-east-2'
image_id = 'ami-0f65671a86f061fcd'
vpc_id = 'vpc-ca1925a2'

default_tag = [{ 'Key' : 'Name', 'Value' : 'Danila/Zubkov'}]
default_filter = [{'Name':'tag:Name', 'Values':['Danila/Zubkov']}]

user_aws_credentials_file = str(Path.home()) + "/.aws/credentials"
exists = os.path.isfile(user_aws_credentials_file)
if exists:
    print("Found AWS credentials file at:", user_aws_credentials_file)
    user_aws_credentials_parse = configparser.ConfigParser()
    user_aws_credentials_parse.read(user_aws_credentials_file)
    aws_access_key_id_value = user_aws_credentials_parse.get('default', 'aws_access_key_id')
    aws_secret_access_key_value = user_aws_credentials_parse.get('default', 'aws_secret_access_key')
else:
    print("File not found at ~/.aws/credentials")
    quit()

ec2_resource = boto3.resource('ec2', aws_access_key_id=aws_access_key_id_value,
                              aws_secret_access_key=aws_secret_access_key_value,
                              region_name=region)

ec2_client = boto3.client('ec2', aws_access_key_id=aws_access_key_id_value,
                          aws_secret_access_key=aws_secret_access_key_value,
                          region_name=region)

az_list = ec2_client.describe_availability_zones()
first_available_az = az_list['AvailabilityZones'][0]['ZoneName']

def create_keypair():
    outfile = open('key.pem', 'w')
    ssh_key_pair = ec2_resource.create_key_pair(KeyName='test_key')
    ssh_key_pair_out = str(ssh_key_pair.key_material)
    outfile.write(ssh_key_pair_out)
    outfile.close()

# check/create key file
if len(ec2_client.describe_key_pairs()['KeyPairs']) > 0:
    for key in ec2_client.describe_key_pairs()['KeyPairs']:
        if key['KeyName'] == 'test_key':
            print('Key exists')
        else:
            create_keypair()
else:
    create_keypair()
    print('Made new one')

try:
    subnet = list(ec2_resource.subnets.filter(Filters=default_filter))[0]
    print('Already exists:', subnet.id)
except:
    subnet = ec2_resource.create_subnet(DryRun = False,
                                        AvailabilityZone= first_available_az,
                                        CidrBlock='172.31.62.0/24',
                                        VpcId=vpc_id
                                        )
    ec2_client.create_tags(Resources=[subnet.id],
                           Tags=default_tag
                           )
    print('None found, made new one:', subnet.id)
# get/create security group
try:
    security_group = list(ec2_resource.security_groups.filter(Filters=default_filter))[0]
    print('Already exists:', security_group.id)
except:
    security_group = ec2_resource.create_security_group(DryRun = False,
                                                        GroupName='group_01',
                                                        Description='allow TCP:22, 80',
                                                        VpcId=vpc_id)
    security_group.authorize_ingress(DryRun=False,
                                     GroupId=security_group.group_id,
                                     IpPermissions=[
                                         {'IpRanges': [
                                             {
                                                 'CidrIp': '0.0.0.0/0',
                                                 'Description': 'Allow from ANY (0.0.0.0/0)'
                                             },
                                         ],
                                            'IpProtocol': 'tcp',
                                             'FromPort': 80,
                                             'ToPort': 80
                                         },
                                         {'IpRanges': [
                                             {
                                                 'CidrIp': '0.0.0.0/0',
                                                 'Description': 'Allow from ANY (0.0.0.0/0)'
                                             },
                                         ],
                                             'IpProtocol': 'tcp',
                                             'FromPort': 22,
                                             'ToPort': 22
                                         }
                                     ])

    ec2_client.create_tags(Resources=[security_group.id],
                           Tags=default_tag
                           )
    print('None found, made new one:',security_group.id)
# get/create instance
try:
    instance_filter = [{'Name':'tag:Name',
                        'Values':['Danila/Zubkov']
                        },
                       {'Name': 'instance-state-name',
                        'Values': ['running']}
                       ]
    all_instances = ec2_resource.instances.filter(Filters=instance_filter)
    if len(list(all_instances)) > 0:
        instances = list(ec2_resource.instances.filter(Filters=instance_filter))[0]
    print('Already exists:', instances.id)
except:
    instances = ec2_resource.create_instances(DryRun = False,
                                              KeyName='test_key',
                                              Placement={
                                                  'AvailabilityZone': first_available_az
                                              },
                                              ImageId=image_id,
                                              InstanceType='t2.micro',
                                              MaxCount=1,
                                              MinCount=1,
                                              NetworkInterfaces=[{'SubnetId': subnet.id,
                                                                  'DeviceIndex': 0,
                                                                  'AssociatePublicIpAddress': True,
                                                                  'Groups': [security_group.group_id]
                                                                  }
                                              ])

    ec2_instance = instances[0]
    ec2_instance.wait_until_running()
    ec2_client.create_tags(Resources=[ec2_instance.id],
                           Tags=default_tag)
    instances = list(ec2_resource.instances.filter(Filters=instance_filter))[0]
    print('None found, created a new one:', ec2_instance.id)
# get/create volume
try:
    volume = list(ec2_resource.volumes.filter(Filters=default_filter))[0]
    print('Already exists:', volume.id)
except:
    volume = ec2_resource.create_volume(AvailabilityZone=first_available_az,
                                        DryRun = False,
                                        Encrypted=False,
                                        Size=1,
                                        VolumeType='standard'
                                        )
    wait_volume = ec2_client.get_waiter('volume_available')
    wait_volume.wait(VolumeIds=[volume.volume_id])
    ec2_client.create_tags(Resources=[volume.id],
                           Tags=default_tag)
    print('None found, created a new one:', volume.id)
# check if attached

if volume.attachments != []:
    print(volume.attachments)
    if volume.attachments[0]['InstanceId'] == instances.id and volume.attachments[0]['State'] == 'attached':
        print(volume.id, 'already attached to', instances.id)
    else:
        print('Attached somewhere else or wrong state.')
        quit()
else:
    attach_volume = volume.attach_to_instance(InstanceId=instances.id,
                                              Device='sdb')
    wait_volume_attach = ec2_client.get_waiter('volume_in_use')
    wait_volume_attach.wait(VolumeIds=[volume.id])
    print(volume.id, ' attached to', instances.id)

try:
    for instance in ec2_resource.instances.all():
        if instance.instance_id == instances.id:
            instance_ip = instance.public_ip_address
except ClientError as e:
    print("Error",e)
    quit()

# init ssh
ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_private_key = paramiko.RSAKey.from_private_key_file('key.pem')

# connect/ssh to an instance, init and mount hdd
try:
    ssh_client.connect(hostname=instance_ip,
                       username="ubuntu",
                       pkey=ssh_private_key)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command("sudo findmnt -n -o SOURCE --target /mnt/disks/xvdb")
    data = ssh_stdout.read().decode('ascii').strip("\n")
    ssh_client.close()
    print(data)
    if data == "/dev/xvdb":
        print('Mounted')
    else:
        try:
            ssh_client.connect(hostname=instance_ip,
                               username="ubuntu",
                               pkey=ssh_private_key)
            init_disk = 'sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/xvdb && ' \
                        'sudo mkdir -p /mnt/disks/xvdb && ' \
                        'sudo mount -o discard,defaults /dev/xvdb /mnt/disks/xvdb && ' \
                        'sudo chmod a+w /mnt/disks/xvdb && ' \
                        'echo UUID=`sudo blkid -s UUID -o value /dev/xvdb` /mnt/disks/xvdb ext4 ' \
                        'discard,defaults,nofail 0 2 | sudo tee -a /etc/fstab && ' \
                        'cat /etc/fstab'
            ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command(init_disk)
            data = ssh_stdout.read().splitlines()
            # for line in data:
            #     print(line.decode())
            # ssh_client.close()
        except Exception as e1:
            print(e1)

except Exception as e:
    print(e)
    quit()


# check/clone repository
try:
    ssh_client.connect(hostname=instance_ip,
                       username="ubuntu",
                       pkey=ssh_private_key)
    check_git_clone = 'ls /mnt/disks/xvdb/devops.co/main-2.py'
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command(check_git_clone)
    data = ssh_stdout.read().decode('ascii').strip("\n")
    ssh_client.close()
    if data:
        print('Already cloned')
    else:
        print('Cloning')
        try:
            ssh_client.connect(hostname=instance_ip,
                               username="ubuntu",
                               pkey=ssh_private_key)
            git_clone = 'git clone https://gitlab.com/s083r/devops.co.git /mnt/disks/xvdb/devops.co'
            ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command(git_clone)
            data = ssh_stdout.read().splitlines()
            for line in data:
                 x = line.decode()
                 print(x)
            ssh_client.close()
        except Exception as e:
            print(e)
except Exception as e:
    print(e)
    quit()

try:
    ssh_client.connect(hostname=instance_ip,
                       username="ubuntu",
                       pkey=ssh_private_key)
    install_py3 = 'sudo apt update && sudo apt install -y python3-pip && ' \
                  'sudo pip3 install -r /mnt/disks/xvdb/devops.co/requirements.txt'
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command(install_py3)
    #data = ssh_stdout.read().decode('ascii').strip("\n")
    data = ssh_stdout.read().splitlines()
    for line in data:
        print(line.decode())
    ssh_client.close()
except Exception as e:
    print(e)
    quit()

try:
    ssh_client.connect(hostname=instance_ip,
                       username="ubuntu",
                       pkey=ssh_private_key)
    install_py3 = 'cd /mnt/disks/xvdb/devops.co/ && sudo python3 task.py --serve'
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_client.exec_command(install_py3)
    #data = ssh_stdout.read().decode('ascii').strip("\n")
    data = ssh_stdout.read().splitlines()
    for line in data:
        print(line.decode())
#    ssh_client.close()
except Exception as e:
    print(e)
    quit()
