import paramiko
import os
from pathlib import Path
import configparser
import boto3
from botocore.exceptions import ClientError

region = 'us-east-2'
image_id = 'ami-0f65671a86f061fcd'
vpc_id = 'vpc-ca1925a2'

default_tag = [{ 'Key' : 'Name', 'Value' : 'Danila/Zubkov'}]
default_filter = [{'Name':'tag:Name', 'Values':['Danila/Zubkov']}]

user_aws_credentials_file = str(Path.home()) + "/.aws/credentials"
exists = os.path.isfile(user_aws_credentials_file)
if exists:
    print("Found AWS credentials file at:", user_aws_credentials_file)
    user_aws_credentials_parse = configparser.ConfigParser()
    user_aws_credentials_parse.read(user_aws_credentials_file)
    aws_access_key_id_value = user_aws_credentials_parse.get('default', 'aws_access_key_id')
    aws_secret_access_key_value = user_aws_credentials_parse.get('default', 'aws_secret_access_key')
else:
    print("File not found at ~/.aws/credentials")
    quit()



ec2_resource = boto3.resource('ec2', aws_access_key_id=aws_access_key_id_value,
                     aws_secret_access_key=aws_secret_access_key_value,
                     region_name='us-east-2')

ec2_client = boto3.client('ec2', aws_access_key_id=aws_access_key_id_value,
                     aws_secret_access_key=aws_secret_access_key_value,
                     region_name='us-east-2')

default_tag = [{ 'Key' : 'Name', 'Value' : 'Danila/Zubkov'}]
default_filter = [{'Name':'tag:Name', 'Values':['Danila/Zubkov']}]

az_list = ec2_client.describe_availability_zones()
first_available_az = az_list['AvailabilityZones'][0]['ZoneName']

