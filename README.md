
Usage:
  - init: python3 task.py (could be issue#1, just restart). AWS credentials are taken from ~/.aws/credentials, region, VPC id and image id are stated in variables at the beginning of main.py;
  - service: python3 task.py --serve

Known issues:
 1. floating error:
    File "/home/s083r/devops.co/main.py", line 167, in <module>
    if volume.attachments[0]['InstanceId'] == instances.id and volume.attachments[0]['State'] == 'attached':
    TypeError: 'NoneType' object is not subscriptable
 2. task.py --serve shuts down when connection closes (no daemon, no screen)
 3. lack of testing
